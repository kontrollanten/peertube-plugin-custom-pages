import { SETTING_NUMBER_OF_PAGES } from '../constants.js'

async function register ({
  registerClientRoute,
  registerSettingsScript,
  peertubeHelpers
}) {
  const settings = await peertubeHelpers.getSettings();
  const numberOfPages = +settings[SETTING_NUMBER_OF_PAGES]

  const pages = Array.from({length: numberOfPages}, (_, i) => i + 1)

  registerSettingsScript({
    isSettingHidden: options => {
      const match = options.setting.name?.match(/\d+/);

      if (match === null || match === undefined) return false

      const [ pageNr ] = match

      return +pageNr > numberOfPages;
    }
  });

  pages.forEach(number => {
    if (!settings[`routePublished${number}`]) {
      return;
    }

    registerClientRoute({
      route: settings[`routePath${number}`],
      onMount: async ({ rootEl }) => {
        const title = settings[`routeTitle${number}`];
        const containerElem = document.createElement('div');
        containerElem.classList = 'plugin-custom-pages margin-content col-md-12 col-xl-6';

        const headerElem = document.createElement('h1');
        headerElem.innerText = title;
        containerElem.appendChild(headerElem);

        const contentElem = document.createElement('div');
        contentElem.innerHTML = await peertubeHelpers.markdownRenderer.enhancedMarkdownToHTML(
          settings[`routeContent${number}`]
        );
        containerElem.appendChild(contentElem);

        rootEl.innerHTML = containerElem.outerHTML;

        const config = await peertubeHelpers.getServerConfig()
        document.title = `${title} - ${config.instance.name}`
      }
    })
  });
}

export {
  register
}
