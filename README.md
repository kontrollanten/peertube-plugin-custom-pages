# Custom pages for PeerTube

Create custom pages on your PeerTube instance. Possibility to set title and markdown content.

1. Install plugin
1. Go to plugin settings and define your routes
1. Visit the route (prepended with /p/, i.e. /p/my-path).
