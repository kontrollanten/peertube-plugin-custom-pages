const path = require("path")

const files = [
  './client/common-client-plugin.js',
  './main.js'
]

let config = files.map(f => ({
  entry: f,

  experiments: {
    outputModule: true
  },

  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: f,
    library: {
      type: f.startsWith('./client/') ? 'module' : 'commonjs-module'
    }
  }
}))

module.exports = config
