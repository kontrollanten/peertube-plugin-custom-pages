import { SETTING_NUMBER_OF_PAGES } from './constants.js'

const registerClientPages = (registerSetting) => (numberOfPages, previousNumberOfPages) => {
  const pages = Array.from({ length: Math.max(numberOfPages, previousNumberOfPages) }, (_, i) => i + 1)

  pages.forEach(number => {
    registerSetting({
      name: `routePath${number}`,
      label: `Page ${number}: Path (/path-name)`,
      type: 'input',
      default: `your-path-${number}`,
      descriptionHTML: 'Without prepended slash. The final route will be \'/p/your-path\'',
      private: false
    });

    registerSetting({
      name: `routeTitle${number}`,
      label: `Page ${number}: Title`,
      type: 'input',
      default: 'My custom title',
      private: false
    });

    registerSetting({
      name: `routeContent${number}`,
      label: `Page ${number}: Content`,
      type: 'markdown-enhanced',
      private: false
    });

    registerSetting({
      name: `routePublished${number}`,
      label: `Page ${number}: Publish`,
      type: 'input-checkbox',
      default: false,
      descriptionHTML: 'Warning: Always consider the content to be public, even though the page isn\'t published.',
      private: false
    });

    registerSetting({
      type: 'html',
      html: '<hr />'
    });
  });
}

async function register ({
  registerSetting,
  registerSettingsScript,
  settingsManager,
}) {
  registerSetting({
    name: SETTING_NUMBER_OF_PAGES,
    label: 'Number of pages',
    default: 5,
    type: 'input',
    descriptionHTML: 'Number of pages you\'d like to create. Decreasing this number will remove eventual existing pages.',
    private: false
  })

  registerSetting({
    type: 'html',
    html: '<hr />'
  });

  const numberOfPages = +(await settingsManager.getSetting(SETTING_NUMBER_OF_PAGES)) || 5
  let previousNumberOfPages = numberOfPages
  registerClientPages(registerSetting, registerSettingsScript)(numberOfPages, previousNumberOfPages)

  settingsManager.onSettingsChange(settings => {
    const numberOfPages = +settings[SETTING_NUMBER_OF_PAGES]
    registerClientPages(registerSetting, registerSettingsScript)(numberOfPages, previousNumberOfPages)
    previousNumberOfPages = numberOfPages
  })
}

async function unregister () {
  return
}

export {
  register,
  unregister
}
